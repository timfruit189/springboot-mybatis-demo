package com.ttx.springboot.mybatisdemo;

import com.ttx.springboot.mybatisdemo.entity.User;
import com.ttx.springboot.mybatisdemo.mapper.UserMapper;
import com.ttx.springboot.mybatisdemo.service.UserService;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author TimFruit
 * @date 19-9-8 上午10:43
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserMapperTests {

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserService userService;


    @Autowired
    SqlSessionFactory sqlSessionFactory;


    @Test
    public void selectUserTest(){
        List<User> userList=userMapper.selectUsers();
        Assert.assertTrue(userList!=null && userList.size()>0);
    }


    /**
     * org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration
     *
     * 使用了new DataSourceTransactionManager(this.dataSource)数据事务管理, 并没有使用mybatis-spring的提供的事务管理
     */
    @Test
    public void transactionTest(){
        User user=new User();
        user.setUserId(99);
        user.setUserName("tx_user");
        user.setAge(11);
        user.setCountry("country");


        try{
            userService.addUserException(user);
        }catch (Exception e){
            e.printStackTrace();
        }


        List<User> users=userMapper.selectUserByName("tx_user");
        Assert.assertTrue(users == null || users.size()==0);
    }


//    @Test
    public void initData(){

        User user;
        for(int i=0;i<100000; i++){
            user=createUser(i);
            userService.addUser(user);
        }

    }



    String[] coutries=new String[]{"中国","美国","英国","法国","俄国"};

    private User createUser(int id){
        User user=new User();

        user.setUserId(id);

        user.setUserName("user"+id);

        int countryIndex=new Random().nextInt(coutries.length);
        user.setCountry(coutries[countryIndex]);

        user.setAge(new Random().nextInt(100)+1);

        Date date=new Date();
        user.setCreateTime(date);
        user.setUpdateTime(date);

        return user;

    }








}
