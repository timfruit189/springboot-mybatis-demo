package com.ttx.springboot.mybatisdemo.mapper;

import com.ttx.springboot.mybatisdemo.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author TimFruit
 * @date 19-9-8 上午8:06
 */
//@Mapper
public interface UserMapper {

    List<User> selectUsers();

    void addUser(@Param("user") User user);

    List<User> selectUserByName(@Param("name") String name);
}
