package com.ttx.springboot.mybatisdemo.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author TimFruit
 * @date 19-9-8 上午11:34
 */
@Configuration
@MapperScan(basePackages = "com.ttx.springboot.mybatisdemo.mapper")  //主要是为了让spring标识和管理
public class MybatisConfig {
}
