package com.ttx.springboot.mybatisdemo.service;

import com.ttx.springboot.mybatisdemo.entity.User;
import com.ttx.springboot.mybatisdemo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author TimFruit
 * @date 19-9-8 上午11:26
 */
@Service
public class UserService {
    @Autowired
    UserMapper userMapper;

    @Transactional
    public void addUserException(User user){
        userMapper.addUser(user);
        throw new RuntimeException("抛出异常测试事务是否有效");
    }


    public void addUser(User user){
        userMapper.addUser(user);
    }

}
