package com.ttx.springboot.mybatisdemo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * MyBatis-Spring-Boot-Starter 官方文档 http://www.mybatis.org/spring-boot-starter/mybatis-spring-boot-autoconfigure/
 * spring-mybatis 官方文档 http://www.mybatis.org/spring/zh/boot.html
 */

@SpringBootApplication
public class MybatisDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisDemoApplication.class, args);
    }

}
